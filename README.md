CLIENT FILES ONLY 

Current version:  1.7

- For Dedicated files go to https://gitlab.com/Medieval/EmuBranchDedicated

- To CHANGE LANGUAGE: Download the EmuConfig.xml at:

https://drive.google.com/open?id=14VxZx6uHZgY_FlybvAAeCdsIkVVYhAmV

Copy the file EmuConfig.xml to the folder where the 7DaysToDie.exe file resides.
You can change the Language in the file to your preferred display language.

- property name="Language" value="Japanese" <---- Change the value here



About:

A special version of Medieval Mod that changes several elements:

Increased chance for ultra rare prefabs to spawn in a random world.
New prefabs
New mobs
New items
User interface changes to cater to displaying the most
types of languages and their respective symbols.

Less rain! Hooray!

Slight increase to certain incredibly rare loot items.

More stuff to come as time goes on.
